package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int i=0, n;
        int max=0, currentNumber;

        System.out.print("input n: ");
        n = scanner.nextInt();

        while (i<n)
        {
            System.out.print("input current number: ");
            currentNumber = scanner.nextInt();

            if(i==0)
            {
                max = currentNumber;
            }
            else
            {
                if(currentNumber>max)
                {
                    max = currentNumber;
                }
            }

            i++;
        }

        System.out.print("max number = "+max);
    }
}
